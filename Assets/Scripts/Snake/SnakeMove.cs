using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeMove : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float rotateSpeed = 200f;

    private float velx = 0f;

    private Rigidbody2D rigidbody2D_snake;
    private PlayerStates playerStates;

    // Start is called before the first frame update
    void Start()
    {
        playerStates = GetComponent<PlayerStates>();
        rigidbody2D_snake = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        velx = Input.GetAxisRaw("Horizontal");
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rigidbody2D_snake.velocity = new Vector2(moveSpeed, 0);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            rigidbody2D_snake.velocity = new Vector2(0, -moveSpeed);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            rigidbody2D_snake.velocity = new Vector2(-moveSpeed, 0);
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            rigidbody2D_snake.velocity = new Vector2(0, moveSpeed);
        }


        if (!playerStates.isAlive ||
            Input.GetKeyUp(KeyCode.RightArrow) ||
            Input.GetKeyUp(KeyCode.LeftArrow)  ||
            Input.GetKeyUp(KeyCode.UpArrow) ||
            Input.GetKeyUp(KeyCode.DownArrow))
        {
            rigidbody2D_snake.velocity = new Vector2(0, 0);
            velx = 0f;
        }
        
    }

    private void FixedUpdate()
    {
        //transform.Translate(Vector3.up * moveSpeed * Time.fixedDeltaTime, Space.Self);
        //transform.Rotate(Vector3.forward * -velx * rotateSpeed * Time.fixedDeltaTime);
    }
}
