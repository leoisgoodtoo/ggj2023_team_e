using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStates : MonoBehaviour
{
    public Vector3 position = Vector3.zero;
    public bool isAlive = true;
    public int healthScore = 100;
    public ConstantsProperty constantsProperty;

    private SnakeTail snakeTailScript;

    private void Start()
    {
        constantsProperty = GetComponent<ConstantsProperty>();
        snakeTailScript = GetComponent<SnakeTail>();
    }

    public void playerStatesInit()
    {
        position = Vector3.zero;
        isAlive = true;
        healthScore = 100;
    }

    public void setHealthScore(string tag)
    {
        if (tag == "Food")
            healthScore = healthScore <= 95 ? healthScore + 5 : 100;
        else if (tag == "Bullet")
            healthScore = healthScore >= 2 ? healthScore - 2 : 0;
        else if (tag == "Mine")
            healthScore = healthScore >= 10 ? healthScore - 10 : 0;

    }
}
