using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeGrowAndRemove : MonoBehaviour
{
    public SnakeTail snakeTail;
    public ConstantsProperty constantsProperty;

    private void Start()
    {
        constantsProperty = GetComponent<ConstantsProperty>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Food"))
        {
            Destroy(collision.gameObject, 0.02f);
            snakeTail.AddTail("Food");
        }
        else if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(collision.gameObject, 0.02f);
            snakeTail.RemoveTail("Bullet");
        }
        else if (collision.gameObject.CompareTag("Mine"))
        {
            Destroy(collision.gameObject, 0.02f);
            snakeTail.RemoveTail("Mine");
        }
        else if (collision.gameObject.CompareTag("Fire"))
        {
            snakeTail.RemoveTail("Fire");
        }
    }
}
