using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeTail : MonoBehaviour
{
    public Transform SnakeTailSprite;
    public float circleDiameter = 1f;

    private List<Transform> snakeTails = new List<Transform>();
    private List<Vector2> positions = new List<Vector2>();

    public ConstantsProperty constantsProperty;
    private PlayerStates playerStates;
    public SceneLoader sceneLoader;

    public Sprite[] bodySprites;
    public TrailRenderer trailRenderer;

    void Start()
    {
        constantsProperty = GetComponent<ConstantsProperty>();
        playerStates = GetComponent<PlayerStates>();
        sceneLoader = GetComponent<SceneLoader>();

        playerStates.playerStatesInit();
        snakeTailInit();
    }

    void Update()
    {
        float distance = ((Vector2)SnakeTailSprite.position - positions[0]).magnitude;

        if (distance > circleDiameter)
        {
            Vector2 direction = ((Vector2)SnakeTailSprite.position - positions[0]).normalized;
            positions.Insert(0, positions[0] + direction * circleDiameter);
            positions.RemoveAt(positions.Count - 1);

            distance -= circleDiameter;
        }

        for (int i = 0; i < snakeTails.Count; i++)
        {
            snakeTails[i].position = Vector2.Lerp(positions[i + 1], positions[i], distance / circleDiameter);
        }
    }

    public void snakeTailInit()
    {
        positions.Clear();
        snakeTails.Clear();

        positions.Add(SnakeTailSprite.position);
        AddTail();
        AddTail();
        AddTail();
    }
    public void AddTail(string TargetTag = "")
    {
        playerStates.setHealthScore(TargetTag);

        Transform tail = Instantiate(SnakeTailSprite, positions[positions.Count - 1], Quaternion.identity, transform);
        var tailSprite = tail.GetComponent<SpriteRenderer>();
        tailSprite.sprite = GetSprite();
        tail.GetComponent<Renderer>().enabled = true;
        snakeTails.Add(tail);
        positions.Add(tail.position);
        tail.name = "tail_" + snakeTails.Count;
        MaintenTrailLength();
    }
    public void RemoveTail(string TargetTag = "")
    {
        playerStates.setHealthScore(TargetTag);

        if (snakeTails.Count <= 0)
        {
            GoResultScene(false);
            return;
        }

        Object.Destroy(transform.Find("tail_" + snakeTails.Count).gameObject);
        snakeTails.RemoveAt(snakeTails.Count - 1);
        positions.RemoveAt(positions.Count - 1);
        MaintenTrailLength();
    }

    void MaintenTrailLength()
    {
        trailRenderer.time = (Mathf.FloorToInt(snakeTails.Count / 4)) * 1.3f;
        //3ball = 1.3s
    }
    Sprite GetSprite()
    {
        int rand = Random.Range(0, bodySprites.Length);
        return bodySprites[rand];
    }

    public void GoResultScene(bool isWin)
    {
        playerStates.isAlive = false;
        ResultStaticValues.isWin = isWin;

        sceneLoader.LoadScene("ResultScene");
    }
}
