using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SnakeHead : MonoBehaviour
{
    // Current Movement Direction
    // (by default it moves to the right)
    Vector2 dir = Vector2.right;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Move in a new Direction?
        if (Input.GetKey(KeyCode.RightArrow))
            dir = Vector2.right;
        else if (Input.GetKey(KeyCode.DownArrow))
            dir = -Vector2.up;    // '-up' means 'down'
        else if (Input.GetKey(KeyCode.LeftArrow))
            dir = -Vector2.right; // '-right' means 'left'
        else if (Input.GetKey(KeyCode.UpArrow))
            dir = Vector2.up;
        else
            dir = Vector2.zero;

        transform.Translate(dir*0.01f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Food"))
        {
            Debug.Log($"[{collision.name}]Enter Food");
            Destroy(collision.gameObject);
        }

        if (collision.CompareTag("Bullet"))
        {
            Debug.Log($"{collision.name}]Enter Bullet");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //if (collision.CompareTag("Food"))
        //{
        //    Debug.Log($"[Exit]{collision.name}");
        //}

        if (collision.CompareTag("Bullet"))
        {
            Debug.Log($"[{collision.name}]Exit Bullet");
        }
    }
}
