using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class SerialTimer : MonoBehaviour
{
    public float duration = 3;
    public float interval = 1;

    public GameObject alert;

    public int serialCount = 0;

    float currentTime;

    public TMP_Text countDownText;

    public GameObject[] bullets;

    public TimerAction[] actions;

    int doneCount = 0;

    void Inint()
    {
        currentTime = duration;
        doneCount = 0;
        foreach (var bullet in bullets)
        {
            var explode = bullet.transform.GetChild(2);
            var bulletDisable = explode.GetComponent<DisableAction>();
            bulletDisable.DonAction += ShootDone;
        }
    }

    void Start()
    {
        Inint();
        StartCoroutine(CountDown());
    }
    IEnumerator CountDown()
    {
        //Alert
        while (currentTime > 0)
        {
            yield return new WaitForSeconds(0.1f);
            if (countDownText != null)
            {
                countDownText.text = $"Alert{currentTime:0.0}s";
            }
            currentTime -= 0.1f;
        }

        //�����wĵ
        alert.SetActive(false);

        //Serial Shoot
        for (int i = 0; i < bullets.Length; i++)
        {
            while (currentTime > 0)
            {
                yield return new WaitForSeconds(0.1f);
                if (countDownText != null)
                {
                    countDownText.text = $"Next{currentTime:0.0}s";
                }
                currentTime -= 0.1f;
            }
            //Debug.Log($"Shoot #{i}");

            float currentFallingTime = 0.5f;
            var fall = bullets[i].transform.GetChild(1);

            fall.gameObject.SetActive(true);

            while (currentFallingTime > 0)
            {
                yield return new WaitForSeconds(0.1f);
                if (countDownText != null)
                {
                    countDownText.text = $"Falling{currentFallingTime:0.0}s";
                }
                currentFallingTime -= 0.1f;
            }
            fall.gameObject.SetActive(false);

            var explode = bullets[i].transform.GetChild(2);
            explode.gameObject.SetActive(true);

            currentTime = interval;
        }
    }
    public void ShootDone()
    {
        doneCount++;
        if (doneCount == bullets.Length)
        {
            Destroy(gameObject);
        }
    }
}
