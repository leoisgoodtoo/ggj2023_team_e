using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float duration = 3;

    float currentTime;

    public TMP_Text countDownText;

    public TimerAction[] actions;

    private void Awake()
    {
        countDownText = GetComponentInChildren<TMP_Text>();
    }

    void OnEnable()
    {
        Inint();
        StartCoroutine(CountDown());
    }

    void Inint()
    {
        currentTime = duration;
    }

    IEnumerator CountDown()
    {
        while (currentTime > 0)
        {
            yield return new WaitForSeconds(0.1f);
            if (countDownText != null)
            {
                countDownText.text = $"{currentTime:0.0}s";
            }

            currentTime -= 0.1f;
        }

        if (countDownText != null)
        {
            countDownText.text = 0 + "s";
        }
        
        foreach (var action in actions)
        {
            action.Do();
        }
    }
}
