using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResultCheck : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public SpriteRenderer winSprite, loseSprite, winFrame, loseFrame, score_s, score_ss, score_sss, scroe_label;

    public AudioSource winAudio, loseAudio;

    // Start is called before the first frame update
    void Start()
    {
        if (ResultStaticValues.isWin)
        {
            scroe_label.enabled = true;
            //textDisplay.text = "WIN";
            winSprite.enabled = true;
            winFrame.enabled = true;
            winAudio.enabled = true;

            if (ResultStaticValues.resultScore == "S")
            {
                score_s.enabled = true;
            }

            if (ResultStaticValues.resultScore == "SS")
            {
                score_ss.enabled = true;
            }

            if (ResultStaticValues.resultScore == "SSS")
            {
                score_sss.enabled = true;
            }
        }
        else
        {
            //textDisplay.text = "LOSE";
            loseSprite.enabled = true;
            //loseFrame.enabled = true;
            loseAudio.enabled = true;
        }

        
    }
}
