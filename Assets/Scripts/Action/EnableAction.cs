using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableAction : TimerAction
{
    public GameObject target;
    public override void Do()
    {
        target.SetActive(true);
    }
}