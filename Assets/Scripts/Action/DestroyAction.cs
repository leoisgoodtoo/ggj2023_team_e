using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAction : TimerAction
{
    public GameObject target;
    public override void Do()
    {
        Destroy(target);
    }
}
