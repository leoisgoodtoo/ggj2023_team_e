using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableAction : TimerAction
{
    public Action DonAction;

    public GameObject[] targets;
    public override void Do()
    {
        DonAction?.Invoke();
        foreach (var target in targets)
        {
            target.SetActive(false);
        }
    }
}
