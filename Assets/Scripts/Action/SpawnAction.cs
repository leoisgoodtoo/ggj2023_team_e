using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAction : TimerAction
{
    public Transform borderLeft;
    public Transform borderRight;

    public Transform borderUp;
    public Transform borderDown;

    float fSpawnX;
    float fSpawnY;

    public GameObject food;
    public override void Do()
    {
        Spawn();
    }

    void Spawn()
    {
        fSpawnX = Random.Range(borderLeft.position.x,
                              borderRight.position.x);
        fSpawnY = Random.Range(borderUp.position.y,
                              borderDown.position.y);

        var newFood = Instantiate(food);
        newFood.transform.position = new Vector2(fSpawnX, fSpawnY);
    }
}
