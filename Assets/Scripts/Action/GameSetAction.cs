using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetAction : TimerAction
{
    public PlayerStates playerStates;

    public override void Do()
    {
        var SnakeTail = playerStates.GetComponent<SnakeTail>();

        if (playerStates.healthScore <= 60)
        {
            Debug.Log("[GameSet]Lose QQ");
            SnakeTail.GoResultScene(false);
        }
        else
        {
            Debug.Log("[GameSet]Win!!");
            if (playerStates.healthScore == 60)
            {
                ResultStaticValues.resultScore = "S";

            }
            if (playerStates.healthScore == 80)
            {
            ResultStaticValues.resultScore = "SS";

            }
            if (playerStates.healthScore == 100)
            {
                ResultStaticValues.resultScore = "SSS";
            }
            SnakeTail.GoResultScene(true);
        }

        
       
    }
}
