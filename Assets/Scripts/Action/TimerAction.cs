using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TimerAction :MonoBehaviour
{
    abstract public void Do();
}
