using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public Transform head;

    // Update is called once per frame
    void Update()
    {
        var cameraPos = head.transform.position;
        cameraPos.z = -10;

        gameObject.transform.position = cameraPos;
        
    }
}
