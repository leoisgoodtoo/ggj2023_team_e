using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class RotateTower : MonoBehaviour
{
    public float angle = 30;
    public float speed = 10f;

    Vector3 direction = Vector3.zero;

    void Start()
    {
        direction = transform.forward;
    }

    void Update()
    {
        transform.Rotate(speed * Time.deltaTime * direction);
        Vector3 Rot = gameObject.transform.localRotation.eulerAngles;
        if (Rot.z >= angle)
        {
            if (Rot.z > 359)
            {

            }

            if (Rot.z <= 359.99999999999999 && Rot.z >= 360 - angle)
            {
            }
            else
            {
                direction *= -1;
            }
        }
    }
}
