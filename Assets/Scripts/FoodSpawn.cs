using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawn : MonoBehaviour
{
    public Transform borderLeft;
    public Transform borderRight;

    public Transform borderUp;
    public Transform borderDown;

    public bool isLoop = false;
    public int spawnCount = 1;

    public float interval;

    float fSpawnX;
    float fSpawnY;

    public GameObject[] spawnObjects;

    void Start()
    {
        if (isLoop)
        {
            InvokeRepeating(nameof(Spawn), 0, interval);
        }
        else
        {
            Spawn();
        }
    }

    void Spawn()
    {
        for (int i = 0; i < spawnCount; i++)
        {
            fSpawnX = Random.Range(borderLeft.position.x,
                                  borderRight.position.x);
            fSpawnY = Random.Range(borderUp.position.y,
                                  borderDown.position.y);

            int rnd = Random.Range(0, spawnObjects.Length);
            var genPrefab = spawnObjects[rnd];

            var newFood = Instantiate(genPrefab);
            newFood.transform.position = new Vector2(fSpawnX, fSpawnY);
        }
    }
}
