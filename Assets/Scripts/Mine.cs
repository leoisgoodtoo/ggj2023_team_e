using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "SnakeHead_1")
        {
            Debug.Log("Mine");
            Destroy(gameObject, 0.02f);
        }
    }
}
