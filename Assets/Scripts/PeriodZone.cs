using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PeriodZone : MonoBehaviour
{
    float currentTime = 0;
    float currentShootPeriodTime = 0;

    public float countDown = 5;
    public float periodTime = 2;

    bool startShoot = false;

    public TMP_Text countDownText;

    public GameObject[] damageObjects;
    public int shootCount = 0;

    private void Start()
    {
        currentTime = countDown;

        foreach (var damageOb in damageObjects)
        {
            damageOb.SetActive(false);
        }

    }

    void FixedUpdate()
    {
        Fire();
        currentShootPeriodTime += Time.fixedDeltaTime;
    }

    void Fire()
    {
        if (currentShootPeriodTime >= periodTime)//�F��g���ɶ�
        {
            currentShootPeriodTime = 0;

            Debug.Log($"shoot#:{shootCount}");
            foreach (var damageOb in damageObjects)
            {
                if (!Object.ReferenceEquals(damageOb, damageObjects[shootCount]))
                {
                    damageOb.SetActive(false);
                }
            }
            damageObjects[shootCount].SetActive(true);
            shootCount++;
            if (shootCount > damageObjects.Length - 1)
            {
                shootCount = 0;
            }
        }
    }
}
