using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollision : MonoBehaviour
{
    private ParticleSystem particle;
    public List<ParticleCollisionEvent> collisionEvents;
    public GameObject explotionPrefab;
    // Start is called before the first frame update
    void Start()
    {
        particle = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = particle.GetCollisionEvents(other, collisionEvents);
        GameObject explotion = Instantiate(explotionPrefab, collisionEvents[0].intersection,Quaternion.identity);
        ParticleSystem p = explotion.GetComponent<ParticleSystem>();
        var pman  = p.main;

        
    }
}
